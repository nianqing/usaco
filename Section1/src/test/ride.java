/*
ID: nwang1
LANG: JAVA
TASK: ride
*/
import java.io.*;
import java.util.*;

/**
 *
 * @author nljjj
 */
public class ride {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        String sComet;
        String sHuman;
        // Use BufferedReader rather than RandomAccessFile; it's much faster
        BufferedReader f = new BufferedReader(new FileReader("ride.in"));
        // input file name goes above
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
        sComet = f.readLine();
        sHuman = f.readLine();

        if ((getNumber(sComet) - getNumber(sHuman)) % 47 == 0) {
            out.println("GO");
        } else {
            out.println("STAY");
        }

        out.close();                                  // close the output file
        System.exit(0);                               // don't omit this!
    }

    static int getNumber(String name) {
        int result = 1;
        for (int i = 0; i < name.length(); i++) {
            result = result * (name.charAt(i) - 'A' + 1);
        }
        return result;
    }

}
