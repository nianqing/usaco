/*
 ID: nwang1
 LANG: JAVA
 TASK: friday
 */
import java.io.*;
import java.util.*;

/**
 *
 * @author nljjj
 */
public class friday {
    int numOfYear;
    int[] count=new int[7];
    static int[] numOfDays= {31,28,31,30,31,30,31,31,30,31,30,31};
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        friday fri=new friday();
        fri.readInput();
        fri.calc();
        //fri.checkOutput();
        fri.printout("friday.out");
        System.exit(0); 
    }

    void readInput() throws FileNotFoundException, IOException {
        BufferedReader f = new BufferedReader(new FileReader("friday.in"));
        numOfYear=Integer.parseInt(f.readLine());
        
    }
    void calc(){
        int whichDay=0;  //sunday 12/31/1899
        for (int i=0;i<numOfYear;i++){
            for (int month=0;month<12;month++){
               whichDay=(whichDay+getDaysDiff(i,month))%7;
               count[whichDay]=count[whichDay]+1;
               //System.out.print(whichDay+" ");
            }
            //System.out.println();
        }
    }
    //calculate a 13th from previous 13th or from 12/31/1899
    int getDaysDiff(int year, int month){
       if (year==0 && month==0) return 13;
       if (month==0) return 31; 
       if (month!=2) return numOfDays[month-1];  //non march month
       year=year+1900;        
       if (year%400==0) return 29;
       if (year%100==0) return 28;
       if (year%4==0) return 29;
       return 28;
    }
    void checkOutput() throws IOException{
        printoutCore(System.out);
    }
    void printout(String fileName) throws IOException{
        PrintStream out = new PrintStream(new File(fileName));
        printoutCore(out);
        out.close();
    }
    void printoutCore(PrintStream out) throws IOException{
        
            out.print(count[6]);
            for (int i=0;i<6;i++){
                out.print(" " + count[i]);
            }
            out.println();
    }
}
