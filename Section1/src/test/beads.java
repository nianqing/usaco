/*
 ID: nwang1
 LANG: JAVA
 TASK: beads
 */
import java.io.*;
import java.util.*;

/**
 *
 * @author nljjj
 */
public class beads {
    String beadsStr;
    int length;
    int breakPoint;
    int beadsCnt=0;
   
    public static void main(String[] args) throws FileNotFoundException, IOException {
        beads b=new beads();
        b.readInput();
        b.calc();
        //b.checkOutput();
        b.printout("beads.out");
        System.exit(0); 
    }

    void readInput() throws FileNotFoundException, IOException {
        BufferedReader f = new BufferedReader(new FileReader("beads.in"));
        length=Integer.parseInt(f.readLine());
        beadsStr=f.readLine();
     
    }
    void calc(){
        int cnt;
        for (int i=0;i<length;i++){
            cnt=countBeads(i,1)+countBeads((i+length-1)%length,-1);
            if (beadsCnt<cnt) beadsCnt=cnt;
        }
        if (beadsCnt>length) beadsCnt=length;
    }
    int countBeads(int start,int direct){
        char color=0;
        char posColor;
        int pos;
        int i;
        for (i=0;i<beadsStr.length();i++){
            pos=(start+direct*i+length)%length;
            posColor=beadsStr.charAt(pos);
            //System.out.println(pos + " " + posColor);
            if (color==0){
                if (posColor!='w') color=posColor;
            }
            else if (posColor != 'w') {
                if (posColor!=color) break;
            }
        }
        return i;
    }
    void checkOutput() throws IOException{
        printoutCore(System.out);
    }
    void printout(String fileName) throws IOException{
        PrintStream out = new PrintStream(new File(fileName));
        printoutCore(out);
        out.close();
    }
    void printoutCore(PrintStream out) throws IOException{
        out.println(beadsCnt);
    }
}
