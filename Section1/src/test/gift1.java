/*
 ID: nwang1
 LANG: JAVA
 TASK: gift1
 */
import java.io.*;
import java.util.*;

/**
 *
 * @author nljjj
 */
public class gift1 {

    int numOfPeople;
    String[] names;
    int[] initMoney;
    int[] numOfPeopleSendMoneyTo;
    int[][] sendMoneyTo;
    int[] change;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        gift1 gift = new gift1();
        gift.readInput();
        gift.checkInput();
        gift.calculateChange();
        gift.checkOutput();
        gift.printout();
        System.exit(0); 
    }

    void readInput() throws FileNotFoundException, IOException {
        BufferedReader f = new BufferedReader(new FileReader("gift1.in"));
        numOfPeople = Integer.parseInt(f.readLine());
        names = new String[numOfPeople];
        initMoney = new int[numOfPeople];
        numOfPeopleSendMoneyTo = new int[numOfPeople];
        sendMoneyTo = new int[numOfPeople][];

        for (int i = 0; i < numOfPeople; i++) {
            names[i] = f.readLine();
        }

        for (int i = 0; i < numOfPeople; i++) {
            int index = getIndex(f.readLine());

            StringTokenizer st = new StringTokenizer(f.readLine());
            initMoney[index] = Integer.parseInt(st.nextToken());
            numOfPeopleSendMoneyTo[index] = Integer.parseInt(st.nextToken());
            sendMoneyTo[index] = new int[numOfPeopleSendMoneyTo[index]];
            for (int j = 0; j < numOfPeopleSendMoneyTo[index]; j++) {
                sendMoneyTo[index][j] = getIndex(f.readLine());
            }

        }
    }

    int getIndex(String name) {
        for (int i = 0; i < numOfPeople; i++) {
            if (names[i].equals(name)) {
                return i;
            }
        }
        return 0;
    }

    void checkInput() {
        for (int i = 0; i < numOfPeople; i++) {
            System.out.print(names[i] + " has " + initMoney[i] + " gives to ");
            for (int j = 0; j < numOfPeopleSendMoneyTo[i]; j++) {
                System.out.print(names[sendMoneyTo[i][j]] + " ");

            }
            System.out.println();
        }
    }
    void calculateChange(){
        change=new int[numOfPeople];
        for (int i=0; i<numOfPeople; i++) {
            if (numOfPeopleSendMoneyTo[i]==0){
                continue;
            }
            int eachPerson=initMoney[i] / numOfPeopleSendMoneyTo[i];
            change[i]=change[i]-eachPerson * numOfPeopleSendMoneyTo[i];
            for (int j=0; j<numOfPeopleSendMoneyTo[i];j++) {
                int person=sendMoneyTo[i][j];
                change[person]=change[person]+eachPerson;
            }
                
            
        }
    }
    void checkOutput() throws IOException{
        printoutCore(System.out);
    }
    void printout() throws IOException{
        printoutCore(new PrintStream(new File("gift1.out")));
        
    }
    void printoutCore(PrintStream out) throws IOException{
        
            for (int i=0; i<numOfPeople;i++){
                out.println(names[i] + " " + change[i]);
            }
    }

}
